Before we start introducing the ideas of rational homotopy theory, we want to give a rundown of some definitions and theorems from homotopy theory and general algebraic topology that we will need later on. These definitions -- and, of course, proofs of the mentioned theorems -- can be found in most textbooks on algebraic topology, such as \cite{hatcherAlgebraicTopology2002}, \cite{mayConciseCourseAlgebraic1999} or \cite{dieckAlgebraicTopology2008}. In the following, any assumed map between topological spaces is continuous, unless explicitly stated otherwise, and we write $I$ for the unit interval $\bck{0,1}\subset\mathbb{R}$ endowed with the subspace topology. Two other kinds of spaces we will often use are the n-spheres $S^{n}$, the subspace of points in $\mathbb{R}^{n+1}$ with distance $1$ to the origin, and the n-disks $D^{n}$, the subspace of points $\mathbb{R}^{n}$ with distance less than or equal to $1$ to the origin. In all of the previous definitions, we assumed $\mathbb{R}^{n}$ to be equipped with the standard topology induced by the Euclidean metric.

\deftion{Pointed Topological Spaces}{
  A pair $\brc{X, x_{0}}$ consisting of a topological space $X$ and a designated point $x_{0}\in X$ is called a \emph{pointed topological space}, and $x_{0}$ is referred to as the \emph{base point} of $X$.

  A map of pointed topological spaces $f\colon\brc{X,x_{0}}\rightarrow\brc{Y,y_{0}}$ is a map $f\colon X\rightarrow Y$ such that $f\brc{x_{0}} = y_{0}$.
}

\deftion{Homotopies of Maps}{
  Let $X, Y$ be topological spaces, $f, g\colon X\rightarrow Y$ be maps. A \emph{homotopy} between $f$ and $g$ (sometimes from $f$ to $g$) is a map $h\colon X\times I\rightarrow Y$ such that the map $h_{0}\colon X\rightarrow Y, x \mapsto h\brc{x,0}\eqqcolon h_{0}\brc{x}$ is equal to f and the map $h_{1}\colon X\rightarrow Y, x \mapsto h\brc{x,1}\eqqcolon h_{1}\brc{x}$ is equal to g. Furthermore, we ask that for any $t\in I$, the analogously defined map $h_{t}$ is continuous and that for any $x\in X$, the map $h_{x}\colon I\rightarrow Y, t \mapsto h_{t}(x) \eqqcolon h_{x}(t)$ is continuous. In less formal language, a homotopy continuously transforms $f$ into $g$, never passing through a non-continuous state.

  A homotopy $h$ is called a \emph{homotopy relative $A$} for some subspace $A\subseteq X$ if $h_{a}$ is constant for any point $a\in A$.

  A map $k$ from $X$ to $Y$ is called \emph{null-homotopic} if there exists a homotopy from $f$ to the map $\mathsc{pt}$ that maps the entirety of $X$ onto a single point in $Y$.
}


\deftion{Homotopy Groups}{
  Let $\brc{S^{n},s_{0}}$ be the pointed $n$-sphere, $\brc{X,x_{0}}$ be a pointed topological space. We define the $n$-homotopy group of $\brc{X,x_{0}}$ as the set of equivalence classes of continuous, base-point preserving maps $\brc{S^{n},s_{0}} \rightarrow \brc{X,x_{0}}$ up to homotopy relative endpoint.
}

\nte{$\pi_{1}$}{The first homotopy group is often referred to as the \emph{fundamental group}.}

\nte{Alternative Definition}{
  Occasionally, it will be more convenient to instead consider maps from the $n$-cube $I^{n}$ into $\brc{X,x_{0}}$, where the entire boundary of $I^{n}$ gets mapped to $x_{0}$, up to homotopy relative boundary. Since $I^{n}$ is homeomorphic to $D^{n}$ and collapsing the boundary of $D^{n}$ to a single point results in an $S^{n}$, this gives us an equivalent definition.
}

\begin{claim}[The Homotopy Groups are Groups]
  We define an operation $\circ$ on the $n$-th homotopy group by concatenation. As it is more convenient for writing out the specifics, we will use the alternative definition for this. Specifically, the product $\bck{\alpha}\circ\bck{\beta}$ of the homotopy equivalence classes $\bck{\alpha}$ and $\bck{\beta}$ is given by the homotopy equivalence class of the map
  \[
    \brc{\alpha\ast\beta}\brc{t_{1},t_{2},\dots,t_{n}}\coloneqq\begin{cases}
      \alpha\brc{2t_{1},t_{2},\dots,t_{n}} & \text{ if } t_{1}\in\bck{0,\frac{1}{2}} \\
      \beta\brc{2t_{1}-1,t_{2},\dots,t_{n}} & \text{ if } t_{1}\in\bck{\frac{1}{2}, 1}. \\
    \end{cases}
  \]
\end{claim}
\begin{proof}
  We quickly outline the proof. Note that since the entire boundary of the $I^{n}$ gets mapped to the point $x_{0}$, we have $\alpha\brc{1,t_{2},\dots,t_{n}} = \beta\brc{1,t_{2},\dots,t_{n}} = x_{0}$ for every $\brc{t_{2},\dots,t_{n}}\in I^{n-1}$, so the map is indeed continuous. Furthermore, if $\alpha^{\prime}\in\bck{\alpha}$ and $\beta^{\prime}\in\bck{\beta}$, then $\alpha^{\prime}\ast\beta^{\prime}\in\bck{\alpha\ast\beta}$, so the operation does not depend on the choice of representatives. To see this pick a homotopy $h_\alpha$ from $\alpha$ to $\alpha^{\prime}$ and a homotopy $h_\beta$ from $\beta$ to $\beta^{\prime}$. Then the map $h_{\alpha\ast\beta}\coloneqq h_{\alpha}\ast h_{\beta}$ is a homotopy from $\alpha\ast\beta$ = $\alpha^{\prime}\ast\beta^{\prime}$. Finally, one can check that this indeed turns the set of homotopy equivalence classes of maps into a group with the constant map as the neutral element, the inverse of a map $\sigma$ being the map $\sigma^{-1}$ given by $\sigma^{-1}\brc{t_{1},\dots,t_{n}}=\sigma\brc{1-t_{1},\dots,t_{n}}$.
\end{proof}

% TODO some text here to lead into next section

\begin{note}[The Higher Homotopy Groups are Abelian]
  While this is generally not true for the fundamental group, one can see that $\pi_{n}$ is abelian for any $n\geq 2$ by defining another operation $\ast_{i}$ on the set of homotopy equivalence classes (obviously, $i\leq n$) that concatenates in the $i$-th instead of the first argument.

  One can then apply the Eckmann-Hilton argument, which tells us that for a set $S$ equipped with two operations $\circ,\bullet\colon S\times S\rightarrow S$ with a common unit $e\in S$ which commute over each other, meaning that
  \[\forall\alpha,\beta,\gamma,\delta\in S \colon\brc{\alpha\circ\beta}\bullet\brc{\gamma\circ\delta} = \brc{\alpha\bullet\beta}\circ\brc{\gamma\bullet\delta},\]
  the operations $\circ$ and $\bullet$ are the same operation, and that operation is commutative and associative. Both $\ast$ and $\ast_{i}$ have the constant map as a unit and they do indeed commute over each other.

  \begin{figure}
    \begin{tikzpicture}[line width=2pt]
      \draw[color=red] (0,0) -- (0,1) -- (1,1) -- (1,0) -- (0,0);
      \node[color=red] (alpha) at (.5,.5) {$\alpha$};
      \draw[color=blue] (1.5,0) -- (1.5,1) -- (2.5,1) -- (2.5,0) -- (1.5,0);
      \node[color=blue] (beta) at (2,.5) {$\beta$};
      \draw[color=green] (0,1.5) -- (0,2.5) -- (1,2.5) -- (1,1.5) -- (0,1.5);
      \node[color=green] (gamma) at (.5,2) {$\gamma$};
      \draw[color=yellow] (1.5,1.5) -- (1.5,2.5) -- (2.5,2.5) -- (2.5,1.5) -- (1.5,1.5);
      \node[color=yellow] (delta) at (2,2) {$\delta$};

      \node (op1) at (3,1.25) {$\overset{\ast_{1}}{\rightarrow}$};

      \draw[color=red] (3.5,1.25) -- (3.5,.75) -- (4.5,.75) -- (4.5,1.25);
      \draw[color=green] (3.5,1.25) -- (3.5,1.75) -- (4.5,1.75) -- (4.5,1.25);
      \draw[color=blue] (5,1.25) -- (5,.75) -- (6,.75) -- (6,1.25);
      \draw[color=yellow] (5,1.25) -- (5,1.75) -- (6,1.75) -- (6,1.25);

      \node (op1) at (6.5,1.25) {$\overset{\ast_{2}}{\rightarrow}$};

      \draw[color=red] (7,1.25) -- (7,.75) -- (7.5,.75);
      \draw[color=green] (7,1.25) -- (7,1.75) -- (7.5,1.75);
      \draw[color=blue] (7.5,.75) -- (8,.75) -- (8,1.25);
      \draw[color=yellow] (8,1.25) -- (8,1.75) -- (7.5,1.75);

      \node (op1) at (8.5,1.25) {$\overset{\ast_{1}}{\leftarrow}$};

      \draw[color=red] (9.5,1) -- (9,1) -- (9,0) -- (9.5,0);
      \draw[color=green] (9.5,2.5) -- (9,2.5) -- (9,1.5) -- (9.5,1.5);
      \draw[color=blue] (9.5,1) -- (10,1) -- (10,0) -- (9.5,0);
      \draw[color=yellow] (9.5,2.5) -- (10,2.5) -- (10,1.5) -- (9.5,1.5);

      \node (op1) at (10.5,1.25) {$\overset{\ast_{2}}{\leftarrow}$};

      \draw[color=red] (11,0) -- (11,1) -- (12,1) -- (12,0) -- (11,0);
      \node[color=red] (alpha) at (11.5,.5) {$\alpha$};
      \draw[color=blue] (12.5,0) -- (12.5,1) -- (13.5,1) -- (13.5,0) -- (12.5,0);
      \node[color=blue] (beta) at (13,.5) {$\beta$};
      \draw[color=green] (11,1.5) -- (11,2.5) -- (12,2.5) -- (12,1.5) -- (11,1.5);
      \node[color=green] (gamma) at (11.5,2) {$\gamma$};
      \draw[color=yellow] (12.5,1.5) -- (12.5,2.5) -- (13.5,2.5) -- (13.5,1.5) -- (12.5,1.5);
      \node[color=yellow] (delta) at (13,2) {$\delta$};

    \end{tikzpicture}
    \caption{A visual outline of the proof that $\ast_{1}$ and $\ast_{2}$ commute over each other for $n=2$.}
  \end{figure}

  This not only tells us that the higher homotopy groups are abelian, but also that the choice of which argument we concatenate in does not actually matter for the definition.
\end{note}

%\ex{Some Examples of Homotopy Groups}{
%  \begin{description}
%    \item[\normalsize label] description
%    \item[\normalsize label] description
%  \end{description}
%}

\deftion{Weak Equivalences}{
  A map of topological spaces $f\colon X \rightarrow Y$ is called a \emph{weak homotopy equivalence} (or sometimes just \emph{weak equivalence}) if it induces isomorphisms on the homotopy groups. In more detail, for an $n\in\mathbb{N}$ we define the induced map $f_{n}\colon\pi_{n}\left(X\right)\rightarrow\pi_{n}\left(Y\right)$ as postcomposition with $f$, so $\left(\sigma\colon S^{1}\rightarrow X\right)\mapsto\left(f\circ\sigma\colon S^{1}\rightarrow Y\right)$. Then f is a weak equivalence if $\forall n\in\mathbb{N}\backslash\set{0}$, the maps $f_{n}$ are group isomorphisms and the map $f_{0}$ is a bijection of pointed sets. Noticeably, any homeomorphism is a weak homotopy equivalence, so spaces with non-isomorphic homotopy groups can not be homeomorphic.
}

\deftion{Path-Connected}{
  A topological space $X$ is called \emph{path-connected} if $\forall x,y\in X$, there is a continuous map $\gamma\colon\bck{0,1}\rightarrow X$ (a path) such that $\gamma(0) = x$ and $\gamma(1) = y$.
}

\clm{The Fundamental Group of a Path-Connected Space}{
  The choice of base-point does not matter when calculating the fundamental group of a path-connected space $X$, so $\pi_{1}\brc{X,x_{0}}\cong\pi_{1}\brc{X,x_{1}}$ for any two $x_{0},x_{1}\in X$.
}
\prf{
  Let $x_{0}, x_{1}$ be two points in $X$ and let $\gamma$ be a path from $x_{0}$ to $x_{1}$. We define a map $\hat{\gamma}\colon\pi_{1}\brc{X, x_{0}}\rightarrow\pi_{1}\brc{X, x_{1}}, \bck{\sigma}\mapsto\bck{\gamma^{-1}\ast\sigma\ast\gamma}$, where $\gamma^{-1}$ is the path from $x_{1}$ to $x_{0}$ defined by $\gamma^{-1}(t) \coloneqq \gamma(1-t)$. Since the concatenation $\gamma\ast\gamma^{-1}$ is null-homotopic (and therefore the neutral element of $\circ$), we have
  \begin{eqnarray*}
    \hat{\gamma}\brc{\bck{\sigma}\circ\bck{\theta}} & = & \hat{\gamma}\brc{\bck{\sigma\ast\theta}} \\
    & = & \bck{\gamma^{-1}\ast\sigma\ast\theta\ast\gamma} \\
    & = & \bck{\gamma^{-1}\ast\sigma\ast\gamma\ast\gamma^{-1}\ast\theta\ast\gamma} \\
    & = & \bck{\gamma^{-1}\ast\sigma\ast\gamma}\circ\bck{\gamma^{-1}\ast\theta\ast\gamma} \\
    & = & \hat{\gamma}\brc{\bck{\sigma}}\circ\hat{\gamma}\brc{\bck{\theta}}
  \end{eqnarray*}
  and
  \begin{eqnarray*}
    \bck{\gamma\ast\gamma^{-1}\ast\sigma\ast\gamma\ast\gamma^{-1}} & = & \bck{\gamma\ast\gamma^{-1}}\circ\bck{\sigma}\circ\bck{\gamma\ast\gamma^{-1}} \\
    & = & \bck{\mathsc{pt}}\circ\bck{\sigma}\circ\bck{\mathsc{pt}} \\
    & = & \bck{\sigma},
  \end{eqnarray*}
  this is a morphism of groups and there is a morphism $\hat{\gamma}^{-1}\colon\pi_{1}\brc{X, x_{1}}\rightarrow\pi_{1}\brc{X, x_{0}}$ such that $\hat{\gamma}^{-1}\hat{\gamma}$ is the identity, and therefore an isomorphism.
}

\deftion{Simply and n-Connected}{
  A topological space $X$ is called \emph{simply connected} if it is path-connected and its first homotopy group is trivial.

  More generally, a topological space $X$ is called \emph{$n$-connected} if it is path-connected and its first $n$ homotopy groups are trivial. A simply connected space is therefore also referred to as a \emph{1-connected space}.
}

\clm{Higher Homotopy Groups of Simply-Connected Spaces}{
  The choice of base-point does not matter when calculating the higher homotopy groups of a path-connected space $X$, so $\pi_{n}\brc{X,x_{0}}\cong\pi_{n}\brc{X,x_{1}}$ for any $n\in\mathbb{N}$ and any two $x_{0},x_{1}\in X$. However, this isomorphism is only canonical if $\pi_{1}\brc{X}$ is trivial, that is if $X$ is simply connected.
}
%\prf{
%
%}

In the following definitions, every topological space is assumed path-connected unless specified otherwise.

%\thrm{Hurewicz Theorem}{
%   %TODO
%  Can be found in Dieck 20.1.
%}
%\prf{
%  yada
%}
%
%\thrm{Whitehead Theorem}{
%  %TODO
%  yada
%}
%\prf{
%    yada
%}

\begin{definition}[CW complex]
  A \emph{relative CW complex} $\left(X,A\right)$ is a pair of hausdorff topological spaces $X$ and $A$ together with an ascending chain of topological subspaces $X_{i}\subseteq X$,
  \[ A = X_{-1} \subseteq X_{0} \subseteq X_{1} \subseteq X_{2} \subseteq  X_{3} \subseteq \cdots, \]
  such that $X=\bigcup_{i\geq-1}X_{i}$ and for all $k\geq0$, $X_{k}$ can be constructed from $X_{k-1}$ by adjoining (or glueing) $k$-cells, e.g. there exists a pushout-diagramm
  \[\begin{tikzcd}
    \coprod_{i\in\mathcal{I}}S^{k-1} \arrow[r] \arrow[d, hook] & X_{k-1} \arrow[d, hook] \\
    \coprod_{i\in\mathcal{I}}D^{k} \arrow[r] & X_{k} \\
  \end{tikzcd}\]
  where the left arrow is just the disjoint union of the inclusions into the boundary. Furthermore, $X$ then has the final topology with regards to the family $X_{i}$, that is the finest topology such that the inclusions $X_{i}\hookrightarrow X$ are continuous and the triangles
  \[\begin{tikzcd}
    X_{i}\arrow[r,hook]\arrow[rd, hook] & X_{j}\arrow[d,hook] \\
    & X \\
  \end{tikzcd}\] commute for every choice $i<j$.
\end{definition}

\thrm{CW Approximation}{
  For every space $Y$  there exists a CW-complex $X$ together with a map $f:X\rightarrow Y$ such that $f$ is a weak equivalence. This is called the \emph{CW approximation} of $Y$, and is unique up to homotopy.
}
\begin{proof}
  Section 8.6 in \cite{dieckAlgebraicTopology2008}.
\end{proof}
%\prf{
%  yada
%}