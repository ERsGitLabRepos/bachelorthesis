In order to understand the machinery we use to take a topological space and \enquote{turn it into} an algebraic object one can calculate with, we first need to introduce some algebraic notions. Throughout this chapter, we assume all structures to be over $\mathbb{Q}$. One can define all of the following notions in terms of modules over a ring as well, but as we do not require that generality, we will not bother with it. The main references for this chapter are \cite{felixRationalHomotopyTheory2001} as well as the STACKS project.
% TODO references -> STACKS project -> Hess -> Moerman -> Felix et al -> Weibel

\deftion{Graded Vector Space}{
  A vector space $V$ is called a \emph{graded vector space} if it can be written as a direct sum of vector spaces $V^{i}$ indexed over the natural numbers,
  \[ V = \bigoplus_{i\in\mathbb{N}}V^{i}.\] We will often write $V^{\ast}$ instead of just $V$ in order to signify we are considering a graded object.

  An element $v\in V$ that is contained within one of the $V^{i}$ is called \emph{homogeneous of degree i}.

  A map of graded vector spaces $\phi: V\rightarrow W$ is said to \emph{be of degree $n$} (or have degree n) if it maps homogeneous elements of degree $i$ to homogeneous elements of degree $i+n$.

  If a graded vector space consists only out of homogeneous elements of the same degree, that is $V^{\ast}=\bigoplus_{i\in\mathbb{N}}V^{i}$ with $V^{\ast}=V^{i}$ for some $i\in\mathbb{N}$ and for all $j\neq i$ we have $V^{j}=0$, then we say that $V^{\ast}$ is concentrated in degree $i$.
}

% TODO maybe add example of Q as a graded VS concentrated in degree 0

% TODO examples (polynomial ring)

\deftion{Cochain Complex}{
  A \emph{Cochain complex} is a graded vector space $C^\ast$ together with a map $d\colon C^{\ast}\rightarrow C^\ast$ of degree 1 such that $d\circ d = 0$. This is also sometimes written as $d^{2}=0$. The map $d$ is usually referred to as the \emph{coboundary map} or as the \emph{differential}, and the elements in its image are referred to as the \emph{coboundaries}, where as the elements in its kernel are called the \emph{cocycles}.
}

% TODO examples

\deftion{Graded (Commutative) Algebras}{
  A graded vector space $A^\ast$ is called a \emph{graded algebra} if there exists a bilinear and associative multiplication $\mu\colon A^{\ast}\times A^{\ast}\rightarrow A^{\ast}$ such that $\mu\brc{A^{n},A^{m}}\subseteq A^{n+m}$ and which has a unit $e\in A^{0}$. For two elements $a, b\in A^\ast$, we will usually write just $ab$ for $\mu(a,b)$. Furthermore, if for all homogeneous elements $a,b\in A^\ast$ we have $ab = \brc{-1}^{|a||b|}ba$, then we call $A^\ast$ \emph{graded commutative}. Some authors will also refer to this property as just commutativity.

  If there exists a map of graded algebras $\epsilon\colon A^{\ast}\rightarrow\mathbb{Q}$, with $\mathbb{Q}$ viewed as a graded algebra concentrated in degree 0, we call $A^{\ast}$ \emph{augmented} and the map $\epsilon$ the \emph{augmentation}.
}

% TODO examples (Tensor Algebra, Exterior Algebra)

\deftion{(Commutative) Differential Graded Algebra}{
  A \emph{differential graded algebra} is a graded algebra that also carries the structure of a cochain complex in a compatible way, so it is equipped with a differential $d\colon A^{\ast}\rightarrow A^{\ast}$ that fulfills the \emph{Leibniz rule}
  \[d\brc{a,b}=\brc{da}b + (-1)^{|a|}a\brc{db}.\]
  We then call $d$ a \emph{derivation}. Furthermore, if the multiplication on $A^\ast$ is graded commutative, we call $A^\ast$ a \emph{commutative differential graded algebra}, or \emph{cdga} for short.
}

% TODO maybe smth with chain complexes

\begin{definition}[The Free Algebra over a (Graded) Vector Space]
  Let $V$ be a graded vector space. We define the \emph{free algebra over $V$} as the graded commutative algebra
  \[
    \Lambda\brc{V}\coloneqq\bigoplus_{n\in\mathbb{N}}\Lambda^{n}\brc{V},\qquad\text{ where } \Lambda^{n}\brc{V}\coloneqq\set{v_{1}v_{2}\cdots v_{n}\mid v_{i}\in V}
  \]
  and the multiplication is graded commutative. Another way to think about this is to view the elements of $V$ (obviously, it is sufficient to just consider any basis) as an alphabet. Then $\Lambda^{n}\brc{V}$ is set of all words of length $n$ over that alphabet, with a handful of special properties
  \begin{enumerate}[(i)]
    \item The words permits a scalar multiplication with elements from $\mathbb{Q}$
    \item The letters have degrees
    \item The positions of adjacent letters can be swapped, inducing a multiplication with $-1$ to the power of the product of the degrees of those letters (this is just graded commutativity)
    \item As a direct result of the previous property, we have $a^{2} = -a^{2}$ for any odd-degree letter, so odd-degree letters cancel themselves.
  \end{enumerate}
  It is important to be aware of the difference between word-length and degree. A word of the shape $v_{1}v_{2}\cdots v_{n}$ will be of length $n$, but its degree will be the sum of the degree of the letters, so generally speaking \emph{not} $n$. We will write $\Lambda^{n}\brc{V}$ for the set of all elements of word-length $n$ and $\Lambda\brc{V}^{n}$ for the set of all elements of degree $n$.
\end{definition}

% TODO also introduce the algebra of polynomial forms on a standard simplex

\deftion{Reduced and Minimal Algebras}{
  Let $A^\ast$ be a cdga. If $A^{0}=\mathbb{Q}$, then we call $A^\ast$ \emph{reduced}. Furthermore, if $A^{r}=0$ for $0<r\leq n$, we call $A^\ast$ \emph{n-reduced}.

  A 1-reduced cdga is called \emph{minimal} if it is isomorphic to a free algebra over a graded vector space $\brc{\Lambda(V^{\ast}),d}$ that fulfills the following (equivalent) conditions
  \begin{enumerate}[(i)]
    \item The graded vector space $V^\ast$ is trivial in degree 0 and 1, $V^{0}=0$ and $V^{1}=0$, so $\brc{\Lambda(V^{\ast}),d}$ is also 1-reduced
    \item The differential $d$ has the property that $d\brc{V^{\ast}}\subset\Lambda^{\geq2}\brc{V^{\ast}}$, so every coboundary is of word-length at least 2.
  \end{enumerate}
}

\deftion{Sullivan Algebras}{
  A cdga $S$ is called a \emph{Sullivan algebra} if it can be written as the free graded commutative algebra over a vector space $V$, that is $S=\brc{\Lambda V, d}$ such that
  \begin{enumerate}[(i)]
    \item $V$ permits a filtration
    \[
      0 = V(-1) \subset V(0) \subset V(1) \subset V(2) \subset \cdots \bigcup_{i\in\mathbb{N}}V(i) = V
    \]
    \item $d(V(k)) \subset \Lambda V(k+1)$
  \end{enumerate}

  % TODO see for example Felix et al chapter 12
  % TODO use either Moerman 6.0.4 or Hess
}

% TODO we should probably also define pure algebras (see Felix et al p. 435)

\thrm{The Main Equivalence}{
  There exists a bijection between rational homotopy types of finite-type rational spaces and isomorphism classes of minimal Sullivan algebras. We will thus often speak of a space's \emph{(minimal) Sullivan model} to signify the image of its rational homotopy type under this bijection.
}

This is maybe the central result of rational homotopy theory, and it is what enables us to do all of the work in the later sections.  There are two proofs of this, one due to Sullivan \cite{sullivanInfinitesimalComputationsTopology1977} and one due to Quillen \cite{quillenRationalHomotopyTheory1969a}, but unfortunately, either is too large in scope and too reliant on abstract machinery to cover here. A more modern treatment can be found in section II of \cite{felixRationalHomotopyTheory2001}.

\begin{proposition}\label{prop:FinTypEllip}
  Let $X$ be a finite-type rational space and $\brc{\Lambda V, d}$ its minimal Sullivan model. Then we have
  \[
    \mathcal{H}^{\ast}\brc{\Lambda V, d}\cong\mathcal{H}^{\ast}(X;\mathbb{Q})\qquad\text{ and }\qquad V\cong\mathrm{Hom}_{\mathbb{Z}}\brc{\pi_{ast}(X),\mathbb{Q}}.
  \]
\end{proposition}
\begin{proof}
  Theorem 10.11 and 15.11 in \cite{felixRationalHomotopyTheory2001}.
\end{proof}

%\ex{The Sullivan Model of $S^{1}$}{
%
%}


