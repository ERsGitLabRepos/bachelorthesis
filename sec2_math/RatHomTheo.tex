While the homotopy groups are a powerful and valuable topological invariant, they are also notoriously hard to compute. A key difficulty here is that there are maps from higher-dimensional spheres into lower-dimensional ones that are not null-homotopic. The most famous example of such a map is probably the Hopf-fibration $\eta\colon S^{3}\rightarrow S^{2}$, a write-up of which can be found in \cite{lyonsElementaryIntroductionHopf}. While it is known that $\pi_{n}(S^{n}) \cong \mathbb{Z}$ and $\pi_{n}(S^{m}) = 0$ for $n<m$, no general rule for the case $n > m$ is known. Consider the following table of homotopy groups of spheres, lifted from \cite{baezThisWeekFinds2009}:

\begin{center}\begin{tabular}{c c c c c | c c c c c}
    $\pi_{3}(S^{2})$ & = & $\mathbb{Z}$ & &
      & $\pi_{9}(S^{5})$ & = & & & $\mathbb{Z}/2\mathbb{Z}$ \\
    $\pi_{5}(S^{3})$ & = & & & $\mathbb{Z}/2\mathbb{Z}$
      & $\pi_{11}(S^{6})$ & = & $\mathbb{Z}$ & & \\
    $\pi_{7}(S^{4})$ & = & $\mathbb{Z}$ & $\times$ & $\mathbb{Z}/12\mathbb{Z}$
      & $\pi_{15}(S^{8})$ & = & $\mathbb{Z}$ & $\times$ & $\mathbb{Z}/120\mathbb{Z}$
\end{tabular}\end{center}

While this is obviously just a (very) small sample, the seeming randomness of the non-free part might motivate one to wonder if it is possible to calculate homotopy groups while \enquote{ignoring} torsion. From an algebraic perspective, one can \enquote{eliminate} torsion in $\mathbb{Z}$-modules by tensoring with $\mathbb{Q}$. But this is an algebraic operation, and its result is not guaranteed to have a sensible interpretation on the level of spaces. For example, consider tensoring $\pi_{1}(S^{1})\cong\mathbb{Z}$ with $\mathbb{Q}$. The result is $\mathbb{Q}$, but if the integer $z\in\mathbb{Z}$ corresponded to the base-point preserving map from $S^{1}$ to $S^{1}$ given by wrapping it around itself $z$ times, what does the fraction $\frac{1}{z}$ represent? There is no base-point preserving map from $S^1$ to itself that covers only a fraction of the circle. The question then raises itself if it is possible to perform the algebraic operation of tensoring with $\mathbb{Q}$ on the level of topological spaces, if one can develop a well-behaved theory of \enquote{tensoring spaces with the rationals}. This is the jump-off point for rational homotopy theory.

\deftion{Rational Space}{
  A simply connected space $X$ is called a \emph{rational space} if all its higher homotopy groups are rational vector spaces, that is
  \[\pi_{i}\left(X\right)\tensor\mathbb{Q} = \pi_{i}\left(X\right)\qquad\forall i > 0.\]
}

\deftion{Finite-type Rational Spaces}{
  Let $X$ be some rational space. If all the homotopy groups of $X$ are finite-dimensional vector spaces, then we say $X$ is of \emph{finite-type}.
}

Since we will only consider simply connected spaces, as by the previous section, we will drop the base point from the notation when discussing the higher homotopy groups.

\deftion{Rational Homotopy Equivalence}{
  A continuous map of topological spaces $f\colon X \rightarrow Y$ is called a \emph{rational homotopy equivalence} (or \emph{rational equivalence} for short) if it induces a linear isomorphism on the \emph{rational homotopy groups}, which is to say
  \[ \pi_{\star}f\tensor\mathbb{Q}\colon\pi_{\star}\left(X\right)\tensor\mathbb{Q}\overset{\cong}{\rightarrow}\pi_{\star}\left(Y\right)\tensor\mathbb{Q}
  \].
}

\nte{Weak and Rational Homotopy Equivalences}{
  Any weak equivalence is a rational equivalence. A map $f:X\rightarrow Y$ is a rational equivalence if and only if it is a weak equivalence.
}

\deftion{Rationalizations}{
  A continuous map $f\colon X \rightarrow Y$ is called a \emph{rationalization} of $X$ if it is a rational homotopy equivalence and $Y$ is a rational space.
}

\begin{theorem}[Rational Hurewicz Theorem]
  Let $X$ be a simply connected space, $r$ some natural number  Then the following two statements hold:
  \begin{enumerate}[(i)]
    \item If $\pi_{i}\brc{X}\tensor\mathbb{Q}=0$ for all $i<r$, then the map  $H\colon\pi_{i}\brc{X}\tensor\mathbb{Q}\overset{\cong}{\rightarrow}\mathcal{H}_{i}\brc{X;\mathbb{Q}}$ induced by the Hurewicz-map is a natural isomorphism for all $i<2r-1$ and a surjection for $i=2r-1$.
    \item The $i$-th homotopy group of $X$ is a rational vector space if and only if the $i$-th homology group is one.
  \end{enumerate}
\end{theorem}
\begin{proof}
  \begin{enumerate}[(i)]
    \item Can be found in \cite{klausQuickProofRational2004}.
    \item This is lemma 8.8 in \cite{griffithsRationalHomotopyTheory2013}.
  \end{enumerate}
\end{proof}

\thrm{Rational Whitehead Theorem}{
  Let $f:X\rightarrow Y$ be a map of simply connected spaces. Then $f$ is a rational equivalence if and only if $\mathcal{H}_{\ast}(f;\mathbb{Q})$, the map induced by $f$ in homology with rational coefficients, is an isomorphism.
}
\prf{
  Theorem 8.6 in \cite{felixRationalHomotopyTheory2001}.
}

\begin{theorem}[Rationalization of a Space]
  For any simply connected space $X$, there exists a rationalization $X_{\mathbb{Q}}$ of $X$, that is to say a rational space $X_{\mathbb{Q}}$ such that
  \begin{enumerate}[(a)]
    \item the map \[\iota_{\mathbb{Q}}\colon X\hookrightarrow X_{\mathbb{Q}}\] is both an inclusion and a rational homotopy equivalence
    \item any other rationalization of $X$ factors over $X_{\mathbb{Q}}$, that is to say if $Y$ is a simply connected, rational space and $f\colon X\rightarrow Y$ is a rational homotopy equivalence, then there exists a unique (up to homotopy) map $f_{\mathbb{Q}}\colon X_{\mathbb{Q}} \rightarrow Y$ such that the diagram below commutes:
    \[\begin{tikzcd}
      X \arrow[rr, "f"]\arrow[rd, hook, "\iota_{\mathbb{Q}}"]
        & % empty node
          & Y \\
      % empty node
        & X_{\mathbb{Q}} \arrow[ur, "f_{\mathbb{Q}}"]
          & % empty node
    \end{tikzcd}\]
  \end{enumerate}
\end{theorem}

This theorem tells us that it is sensible to speak of \emph{the} rationalization of a space, since the space $X_{\mathbb{Q}}$ from the above theorem is unique up to homeomorphism (to see this, assume another space with the same property and plug the two triangle diagrams together). We will not explicitly prove this theorem due to size constraints, but hopefully, the exemplary constructions below will give a good intuition as to why it holds true.

We will now explicitly construct the rationalization of the 1-sphere and use it to give an intuition as to how one can construct the rationalization of an arbitrary simply connected space. Note that, since the 1-sphere is not a simply connected space itself, it is technically not covered by the above theorem. However, it is an example of a non-simply connected space that still permits a rationalization, since its fundamental group is abelian.\footnote{One can extend rational homotopy theory to 0-connected spaces that satisfy certain properties, see \cite{gomez-tatoRationalHomotopyTheory1999}. However, for scope reasons, we will not cover that case here as well.} For a more in-depth and rigorous account that covers the higher dimensional spheres, CW-complexes and arbitrary simply connected spaces as well, see chapter 9 of \cite{felixRationalHomotopyTheory2001}.

\begin{example}[Rationalization of the 1-Sphere]
  A way of characterizing that an abelian group $\mathcal{A}$ permits the structure of a $\mathbb{Q}$-vector space is that the equation $z\alpha = \beta$ has a unique solution $\alpha\in\mathcal{A}$ for all $z\in\mathbb{Z}\backslash\set{0}$, $\beta\in\mathcal{A}$ \cite{griffithsRationalHomotopyTheory2013}. More informally, we can \enquote{divide by any integer} in $\mathcal{A}$. Since the higher homotopy groups of the 1-sphere are trivial, we can focus on only the fundamental group when discussing the rationalization.

  Consider that we can identify $\pi_{1}(S^{1})$ with the integers, where the (homotopy equivalence class of the) map $f_{z}\colon S^{1} \rightarrow S^{1}$ wrapping the circle around itself $z$ times is identified with the integer $z$. From this, we can use our understanding of how division by integers works to derive a geometric intuition of what the rationalization $S^{1}_{\mathbb{Q}}$ of the $S^{1}$ needs to look like as a space.

  To develop that intuition, let us return to the equation $z\alpha = \beta$ again, and let $\beta$ be the identity on $S^{1}$. Then the equation is solvable if there is a base-point preserving map $S^{1}\rightarrow S^{1}_{\mathbb{Q}}$ that corresponds to \enquote{walking} a $\sfrac{1}{z}$-th of the unit circle. In other words: for each integer $z$, there needs to be a map from the 1-sphere into the rationalization such that \enquote{walking around} the map corresponding to the identity is homotopic to \enquote{walking around} that map $z$ times. Furthermore, these need to be compatible with each other, so \enquote{walking around} the \enquote{$z$-copy} once has to be homotopic to traversing the \enquote{$kz$-copy} $k$ times.
  % TODO one final sentence

  \begin{figure}
    \[\begin{tikzpicture}
      \draw (0,0) ellipse (.4 and .8);
      \node (1) at (0,-1.2) {\enquote{1}};
      \draw (1.5,0) ellipse (.5 and 1);
      \node (2) at (1.5,-1.4) {\enquote{$\frac{1}{2}$}};
      \draw (3.2,0) ellipse (.6 and 1.2);
      \node (3) at (3.2,-1.6) {\enquote{$\frac{1}{6}$}};
      \draw (5.1,0) ellipse (.7 and 1.4);
      \node (4) at (5.1,-1.8) {\enquote{$\frac{1}{24}$}};
      \draw (7.2,0) ellipse (.8 and 1.6);
      \node (5) at (7.2,-2) {\enquote{$\frac{1}{120}$}};
      \node (A) at (8.6,0) {$\cdots$};
    \end{tikzpicture}\]
    \caption{\small Constructing $S^{1}_{\mathbb{Q}}$, step 1: Adding copies of the $S^{1}$.}\label{fig:SQ_only-circles}
  \end{figure}

  Now, we face the issue that the maps of the $S^{1}$ onto these different copies are not homotopic to each other (and therefore, we can not really speak of one of them corresponding to $z$ times another one). To rectify this issue, we glue a hollow cylinder in between each copy of the $S^{1}$ and its successor, the \enquote{left end} of the cylinder being glued to the $z$-th copy of $S^{1}$ via the identity and the \enquote{right end} being glued to the successor by being wrapped around it $z+1$ times.

  \begin{figure}
    % TODO fill out cylinders
    \[\begin{tikzpicture}
      \draw (0,0) ellipse (.4 and .8);
      \node (1) at (0,-1.2) {\enquote{1}};
      \draw (1.5,1) arc [start angle=90, end angle=-90, x radius=.5, y radius=1];
      \draw [dashed] (1.5,1) arc [start angle=90, end angle=-90, x radius=-.5, y radius=1];
      \node (2) at (1.5,-1.4) {\enquote{$\frac{1}{2}$}};
      \draw (3.2,1.2) arc [start angle=90, end angle=-90, x radius=.6, y radius=1.2];
      \draw [dashed] (3.2,1.2) arc [start angle=90, end angle=-90, x radius=-.6, y radius=1.2];
      \node (3) at (3.2,-1.6) {\enquote{$\frac{1}{6}$}};
      \draw (5.1,1.4) arc [start angle=90, end angle=-90, x radius=.7, y radius=1.4];
      \draw [dashed] (5.1,1.4) arc [start angle=90, end angle=-90, x radius=-.7, y radius=1.4];
      \node (4) at (5.1,-1.8) {\enquote{$\frac{1}{24}$}};
      \draw (7.2,1.6) arc [start angle=90, end angle=-90, x radius=.8, y radius=1.6];
      \draw [dashed] (7.2,1.6) arc [start angle=90, end angle=-90, x radius=-.8, y radius=1.6];
      \node (5) at (7.2,-2) {\enquote{$\frac{1}{120}$}};
      \node (A) at (8.6,0) {$\cdots$};

      \draw (0,.8) -- (1.5,1) -- (3.2,1.2) -- (5.1,1.4) -- (7.2,1.6);
      \draw (0,-.8) -- (1.5,-1) -- (3.2,-1.2) -- (5.1,-1.4) -- (7.2,-1.6);
    \end{tikzpicture}\]
    \caption{Constructing $S^{1}_{\mathbb{Q}}$, step 2: Gluing cylinders inbetween the copies of the $S^{1}$.}\label{fig:SQ_with-cyls}
  \end{figure}

  So we end up with a space that looks like some sort of infinite twisted telescope. Let us investigate how this space actually fulfills our algebraic requirement from above. Note that by the way we glued the cylinders in, going around the first circle once is homotopic to going around the second circle twice, and going around the second circle twice is homotopic to going around the third circle thrice, so going around the first circle once is homotopic to going around the third circle six times. In more general terms: going around the first circle once is homotopic to going around the $z$-th circle $z!$ times, and going around the $n$-th circle once is going around the $z$-th circle $\frac{z!}{n!}$ times. So it seems this space allows us to solve all the above equations.

  Let us now attempt to turn this intuition into a rigorous description. The first thing we notice is, that since all maps from the $S^{1}$ into our space are supposed to be base-point preserving, we can't actually just use disjoint copies of the $S^{1}$, but need to identify their base point with each other somehow. Starting out with $S^{1}\brc{1} = S^{1}$, we build the rationalization inductively by defining $S^{1}\brc{r+1}$ as the pushout
  \[\begin{tikzcd}
    S^{1}\arrow[r, "\phi"]\arrow[d, "i", hook] & S^{1}\brc{r}\vee S^{1}\arrow[d] \\
    D^{2}\arrow[r] & S^{1}(r+1),
  \end{tikzcd}\]
  where $i$ is the inclusion into the boundary and $\phi$ is the composition
  \[\begin{tikzcd}S^{1}\arrow[r] & S^{1}\vee S^{1} \arrow[r, "j\vee g_{r}"]& S^{1}(r)\vee S^{1},\end{tikzcd}\]
  with the first map being the collapse of the equator, $j$ being the inclusion into the $r$-th copy of the $S^{1}$ in $S^{1}(r)$ and $g_{r}$ being the map wrapping the sphere $r+1$ times around itself. This yields the definition $S^{1}\brc{k} = \bigvee_{i=1}^{k}S^{1}\cup_{g}\coprod_{i=1}^{k-1}D^{2}$, where $\cup_{g}$ is supposed to signify that we glue the disjoint union of disks into the smash product of spheres along the maps $g_{i}$. Since $S^{1}\brc{r+1}$ is created from $S^{1}\brc{r}$ by adjoining cells, this defines the structure of a CW-complex $S^{1}_{\mathbb{Q}} \coloneqq \bigvee_{k \geq 1} S^{1} \cup_{g} \coprod_{k\geq 2} D^{2}$. This space is exactly the rationalization of $S^{1}$.

  Why is this the case? Note that at any point in the construction, the inclusion $i_{r}\colon S^{1}\hookrightarrow S^{1}(r)$ into the $r$-th copy of the $S^{1}$ is a weak equivalence, as we can collapse the \enquote{telescope} to the last sphere, so $\pi_{1}\brc{S^{1}(r)}=\mathbb{Z}$. Furthermore, consider the maps $i_{r}\colon S^{1}\hookrightarrow S^{1}(r+1)$ and $i_{r+1}\colon S^{1}\hookrightarrow S^{1}(r)$. By construction, $i_{r}$ is homotopic to $(r+1)$ times $i_{r+1}$, so $\bck{i_{r}} = (r+1)\bck{i_{r+1}}\in\pi_{1}(S^{1}(r+1))$. That means we can divide the class $\bck{i_{r}}$ by $(r+1)$ in $\pi_{1}(S^{1}(r+1))$ and the map induced by the inclusion $S^{1}(r)\hookrightarrow S^{1}(r+1)$ is given by multiplication with $(r+1)$. Since the fundamental group and the first homology group of the $S^{1}$ are isomorphic, we get a filtered colimit of homology groups
  \[\begin{tikzcd}
    \mathbb{Z}\arrow[r, "\cdot 2"] & \mathbb{Z}\arrow[r, "\cdot 3"] & \mathbb{Z}\arrow[r, "\cdot 4"] & \mathbb{Z}\arrow[r, "\cdot 5"] & \mathbb{Z}\arrow[r, "\cdot 6"] & \cdots.
  \end{tikzcd}\]
  Homology commutes with colimits, and doing this in every degree yields that
  \[
    \mathcal{H}_{n}\brc{S^{1}_{\mathbb{Q}}}=\begin{cases}
      \mathbb{Q}, & n = 1 \\
      0, & \text{otherwise,}
    \end{cases}
  \]
  and by the second version of the rational Hurwicz theorem, $S^{1}_{\mathbb{Q}}$ is a rational space. But then the rational Whitehead theorem tells us that the inclusion $i_{1}\colon S^{1}\hookrightarrow S^{1}_{\mathbb{Q}}$ into the initial sphere is a rationalization. The only thing left to show is that every other rationalization factors over it. We will not prove this right now, but prove a more general statement in a second.

  Having defined the rational 1-sphere, we can define the rational 2-disk $D^{2}_{\mathbb{Q}}$ analogous to how we can define the regular $D^{2}$: as the cone of $S^{1}_{\mathbb{Q}}$, that is as the space $S^{1}_{\mathbb{Q}}\times I / S^{1}_{\mathbb{Q}}\times\set{0}$, where $I$ is the unit interval.
\end{example}

\nte{Rational Spheres and Disks}{
  Analogous to the constructions above, we can construct $S^{n}_{\mathbb{Q}} = \bigvee_{k \geq 1} S^{n} \cup_{g} \coprod_{k\geq 2} D^{n+1}$ and $D^{n+1}_{\mathbb{Q}} = S^{n}_{\mathbb{Q}}\times I / S^{n}_{\mathbb{Q}}\times\set{0}$, the rational $n$-sphere and rational $n$-disk.
}

\begin{lemma}
  Let $X$ be a rational space and $f\colon S^{n}\rightarrow X$ be a map. Then $f$ factors over $S^{n}_{\mathbb{Q}}$, that is there exists an extension $f_\mathbb{Q}\colon S^{n}_{\mathbb{Q}}\rightarrow X$ such that the diagram
  \[\begin{tikzcd}
    S^{n}\arrow[r]\arrow[rd, "f"] & S^{n}_{\mathbb{Q}}\arrow[d,"f_{\mathbb{Q}}"]\\
    & X
  \end{tikzcd}\]
  commutes. This map is determined up to homotopy, that is homotopic maps have homotopic extensions and for any homotopy class of maps from $S^{n}$ to $X$, their extension is unique up to homotopy.
\end{lemma}
\begin{proof}
  Note that the map $f$ represents an element $\bck{\alpha}$ in the $n$-th homotopy group of $X$. Since the $n$-th homotopy group is a rational vector space by assumption, there exist elements $\frac{1}{2}\bck{\alpha}, \frac{1}{3}\bck{\alpha}, \dots$ in it. We pick representatives of these elements and suggestively call them $\frac{1}{2}f, \frac{1}{3}f$ etc. We then extend $f$ onto $S^{1}_{\mathbb{Q}}$ by defining $f_{\mathbb{Q}}$ to be equal to $\frac{1}{k!}f$ on the $k$-th copy of the $S^{n}$ in $S^{n}_{\mathbb{Q}}$. One can then verify that this a well-defined map and that it has the desired properties.
\end{proof}

\begin{example}[The Rationalization of a CW-Complex]
  Having constructed the rationalizations of the $n$-spheres and -disks, we now use this to construct the rationalization of an arbitrary simply connected CW-complex $X$. The basic idea is to replace the copies of $S^{n}$ and $D^{n}$ in the push-outs with their rationalizations.

  We again define the rationalization $X_{\mathbb{Q}}$ inductively, by rationalizing every space $X_i$ in the ascending chain of the CW-structure of $X$. Since $X$ is simply connected, we can assume it contains no 1-cells, so $X^{0}_{\mathbb{Q}}$ and $X^{1}_{\mathbb{Q}}$ are just the single-point space. Now, let
  \[\begin{tikzcd}
  \coprod_{i\in I}S^{k} \arrow[r, "\coprod g_{i}"]\arrow[d] & X^{k}\arrow[d] \\
  \coprod_{i\in I}D^{k+1} \arrow[r] & X^{k+1}
  \end{tikzcd}\]
  be the pushout defining $X^{k+1}$ and assume the rationalization of $X^{k}$ has already been constructed. We can post-compose the rationalization map $X^{k}\rightarrow X^{k}_{\mathbb{Q}}$ to the attaching maps $g_{i}$ from the $S^{k}$ to $X^{k}$ in the pushout diagram defining $X^{k+1}$, and by the previous lemma, this yields attaching maps $g_{i}^{\prime}$ from $S^{k}_{\mathbb{Q}}$ to $X^{k}_{\mathbb{Q}}$. Thus, we can construct a pushout
  \[\begin{tikzcd}
    \coprod_{i\in I} S^{k}_{\mathbb{Q}} \arrow[r, "\coprod g_{i}^{\prime}"]\arrow[d] & X^{k}_{\mathbb{Q}}\arrow[d] \\
    \coprod_{i\in I} D^{k+1}_{\mathbb{Q}}\arrow[r] & X^{k+1}_{\mathbb{Q}}.
  \end{tikzcd}\]
  We can plug together these two pushouts and, by the universal property of the pushout, this yields a unique map $X^{k+1}\rightarrow X^{k+1}_{\mathbb{Q}}$. One can then show that both $X^{k+1}_{\mathbb{Q}}$ is rational and that this map is a rationalization using the Mayer-Vietoris sequence for push-outs and the 5-lemma.
\end{example}

\begin{corollary}
  Every simply connected space $X$ admits a rationalization.
\end{corollary}
\begin{proof}
  Let $Y\overset{f}{\rightarrow}X$ be a CW-approximation of $X$ and let $Y\overset{\phi}{\rightarrow}Y_{\mathbb{Q}}$ be the rationalization of that approximation. We define $X_{\mathbb{Q}}\coloneqq X\cup_{f}\brc{Y\times I}\cup_{\phi}Y_{\mathbb{Q}}$, with the inclusion $X\overset{\psi}{\hookrightarrow}X_{\mathbb{Q}}$ into the \enquote{starting point}.

  Using excision, we can see that \[\mathcal{H}_{ast}\brc{X_{\mathbb{Q}}, Y_{\mathbb{Q}}} \cong \mathcal{H}_{ast}\brc{X\cup_{f}\brc{Y\times I}, Y\times1} = 0.\]
  By the long exact sequence of homology, we thus get $\mathcal{H}_{ast}(X_{\mathbb{Q}})\cong\mathcal{H}_{ast}(Y_{\mathbb{Q}})$, and by rational Hurewicz, $X_{\mathbb{Q}}$ is a rational space. Using again the long exact sequence of homology, the five lemma and the already known isomorphisms of two of its parts, we get that $\mathcal{H}_{ast}(X_{\mathbb{Q}}, X;\mathbb{Q})\cong\mathcal{H}_{ast}(Y_{\mathbb{Q}},Y;\mathbb{Q})=0$, and therefore $\psi$ induces an isomorphism in rational homology. But then the rational Whitehead theorem tells us that $X_{\mathbb{Q}}$ is a rationalization of $X$.
\end{proof}
