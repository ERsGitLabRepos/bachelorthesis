Before we start talking about what it means for a computation problem to be \enquote{efficiently computable}, we first need to clarify what it means for something to be computable in the first place. To do so, we introduce a basic model of computation, the Turing machine. We first give an informal description of how a Turing machine works and what its internals look like, before moving on to a more formal definition. However, Turing machines have a very nice theoretical property that allows us to argue about them on a very high level of abstraction, so the technical details of the definition are often regarded as not as important as a good understanding of how they work.

A Turing machine consists out of a control unit, a read-write head and an infinite-length tape to write on and read from, divided into an infinite amount of uniformly sized cells. Note that the tape is only of infinite length to free us from concerns about space constraints -- the machine will never run out of tape space during execution --, but as a result of how a Turing machine works (see further below), it does not allow us to actually surpass the capabilities of an actual computer (which obviously does not have access to an unlimited amount of memory), so at any given point during execution, only a finite amount of cells can actually have been written to/read from.

The control unit consists out of a finite amount of states and possible transitions between them. This can be thought of as a directed graph that permits loops and multi-edges. One state is the designated \emph{starting state}, and two or more designated \emph{halting states}, which are either \emph{accepting} or \emph{rejecting}.

At the start of execution, the tape contains the \emph{input}, a finite string of symbols (for example the binary representation of an integer), one symbol per cell. The read-write head points to the start of the input, the control unit is in the starting state. The start of the input, however, is not the start as the tape, but \enquote{somewhere in the middle}, that is to say that at the beginning, there is an infinite amount of tape space to both the left and the right of the input. It might be helpful to view the tape cells as indexed by the integers -- there is no first cell, same as there is no last cell --, and the input string just starts at some integer.

The execution itself consists of discrete steps. At the beginning of each step, the read-write head reads the content of the cell it is currently pointing at. This then triggers a state transition, even though transitions into the state the machine is already in are allowed (that is to say the machine does not necessarily change state during a transition). During the transition, the machine may write a symbol to the cell the read-write head is currently pointing at, replacing any symbol already inside that cell, and/or move the read-write head one cell to the left or right. If the machine both writes a symbol to the tape and moves the head, the writing is always done first, the moving second. If the transition includes a change of state, this is done last.

If, at any point during execution, the machine transitions into one of the designated halting states, it immediately stops executing. We say that the input was either \emph{accepted} or \emph{rejected}, depending on whether the machine stopped in an accepting or rejecting halting state. Any string of symbols still left on the tape is referred to as the \emph{output} of the execution. However, it is also possible for the machine to never halt, for example when, during execution, the machine enters an ever-repeating circular sequence of transitions into non-halting states. We then say that the machine \emph{loops}.

\begin{figure}
  \[\begin{tikzpicture}
    \tikzstyle{every path}=[very thick]

    \edef\sizetape{0.7cm}
    \tikzstyle{tmtape}=[draw,minimum size=\sizetape]
    \tikzstyle{tmhead}=[arrow box,draw,minimum size=.5cm,arrow box
    arrows={east:.25cm, west:0.25cm}]

    %% Draw TM tape
    \begin{scope}[start chain=1 going right,node distance=-0.15mm]
      \node [on chain=1,tmtape,draw=none] {$\ldots$};
      \node [on chain=1,tmtape] {\blank};
      \node [on chain=1,tmtape] (input) {0};
      \node [on chain=1,tmtape] {0};
      \node [on chain=1,tmtape] {1};
      \node [on chain=1,tmtape] {1};
      \node [on chain=1,tmtape] {1};
      \node [on chain=1,tmtape] {1};
      \node [on chain=1,tmtape] {\blank};
      \node [on chain=1,tmtape] {\blank};
      \node [on chain=1,tmtape,draw=none] {$\ldots$};
      \node [on chain=1] {\textbf{Input/Output Tape}};
    \end{scope}

    %% Draw TM Finite Control
    \begin{scope}
      \tikzset{
        ->, % makes the edges directed
        node distance=2cm, % specifies the minimum distance between two nodes. Change if necessary.
        every state/.style={thick, fill=gray!10}, % sets the properties for each ’state’ node
        initial text=$ $, % sets the text that appears on the start arrow
      }

      \node[state, initial] (q0) at (2,-6) {$q_{0}$};
      \node[state, right = of q0] (q1) {$q_{1}$};
      \node[state, right = of q1] (q2) {$q_{2}$};
      \node[state, right = of q2] (q3) {$q_{3}$};
      \node[state, below = of q3] (q4) {$q_{4}$};
      \node[state, below = of q2] (q5) {$q_{5}$};
      \node[state, below = of q1] (q6) {$q_{6}$};
      \node[state, below = of q5, accepting] (q7) {$q_{7}$};
      \node[right = of q3, xshift=-.75cm] (dummy) {};
      \node[above = of q1, yshift=-1.25cm] (dummy2) {};

      \draw (q0) edge[loop below] node{0|0, R} (q0)
            (q0) edge[above] node{1|1, R} (q1)
            (q1) edge[loop above] node{1|1, R} (q1)
            (q1) edge[above] node{\blank|\blank, L} (q2)
            (q2) edge[above] node{1|\blank, L} (q3)
            (q3) edge[loop right] node{$\begin{matrix}0|0, L \\ 1|1, L\end{matrix}$} (q3)
            (q3) edge[right] node{\blank|\blank, R} (q4)
            (q4) edge[above] node{0|\blank, R} (q5)
            (q5) edge node{$\begin{matrix}0|0, R \\ 1|1, R\end{matrix}$} (q6)
            (q6) edge[loop left] node{$\begin{matrix}0|0, R \\ 1|1, R\end{matrix}$} (q6)
            (q6) edge[left] node{\blank|\blank, L} (q2)
            (q5) edge[right] node{\blank|\blank, R} (q7);

      \node[rounded corners,draw=black,thick,fit=(q0) (q1) (q2) (q3) (q4) (q5) (q6) (q7) (dummy) (dummy2),
      label=below:\textbf{Control Unit}, inner sep=1em] (fsbox)
      {};
    \end{scope}

    %% Draw TM head below (input) tape cell
    \node [tmhead,yshift=-.3cm] at (input.south) (head) {$q_0$};

    % Link Finite Control with Head
    \path[-,draw] (fsbox.north) .. controls (4.5,-1) and (0,-2) .. node[right]
    (headlinetext)
    {}
    (head.south);
    \node[xshift=2cm,yshift=.5cm] at (headlinetext)
    {\begin{tabular}{c}
        \textbf{Read-Write Head} \\
        \textbf{(moves in both directions)}
    \end{tabular}};

  \end{tikzpicture}\]
  \caption{\footnotesize An illustration of a Turing Machine. The transitions should be read as \enquote{\emph{input}|\emph{output}, \emph{head movement}}. The state diagram of the control unit was lifted from \cite{wagnerVorlesungsskriptTheoretischeGrundlagenWS11/12} and recognizes the language $L^{=}\coloneqq\set{0^{n}1^{n}\mid n\geq1}$. There are a couple of transitions not drawn, all of which lead into the (also not drawn) rejecting state.}
\end{figure}

\deftion{Turing Machine}{
  A \emph{(deterministic) Turing Machine} $\TM$ is a 7-tuple $\left(Q, \Sigma, \Gamma, \delta, q_{0}, q_{\mathsf{accept}}, q_{\mathsf{reject}}\right)$, consisting out of
  \begin{itemize}
    \item A finite set of states $Q$
    \item A finite set of symbols $\Sigma$, the \emph{input alphabet}
    \item A finite set of symbols $\Gamma$, the \emph{tape alphabet}, containing $\Sigma$, the blank symbol \blank\ and any other symbol the machine may write to the tape
    \item A transition function $\delta\colon Q \times \Gamma \rightarrow Q \times \Gamma \times \set{L,R,N}$
    \item A start-state $q_{0}\in Q$
    \item An accept-state $q_{\mathsf{accept}}\in Q$
    \item A reject-state $q_{\mathsf{reject}}\in Q$
  \end{itemize}
  The input alphabet may not contain the blank symbol. The transition function takes the current state and the symbol written in the cell the read-write head is currently pointing to and maps it to a triple of the new state of the machine (can be the one the machine is already in), the symbol that is to be written to the tape cell (can be the one read from the tape) and the direction to move the read-write head to (left, right or no movement).
}

Note that in the literature one can find several slightly different, but ultimately equivalent, definitions. Some may include multiple single-purpose tapes (for example one for input, one for output and one for calculations), each one with its own dedicated read, write or read-write head. Some define the tape cells as indexed by the natural numbers, that is there is a unique starting cell. Some may include a \enquote{square} tape, that is one where the cells are indexed by $\mathbb{Z}\times \mathbb{Z}$, and where the read-write head can move up and down as well as left and right. Some may include a fixed input-alphabet. Some may not include rejecting final states, only accepting or even only halting ones.

However, these technical details do not actually matter a great deal: it is a central notion of theoretical computer science that not only is every model of classical computation as powerful as any other one, any model can also simulate any other model efficiently. This is (a slight restriction of) the so-called \emph{extended Church-Turing thesis}. In order to properly state the extended Church-Turing thesis, we first need to specify what we mean when we say \enquote{efficiently}.

\deftion{Polynomial Execution Time}{
  A Turing machine $\TM$ halts (or terminates) in \emph{polynomial time} if there is a polynomial $p$ such that $\TM$ halts on any input string $\omega$ after at most $p\left(|\omega|\right)$ steps.
}

A bit more reasoning on why it has become custom to interpret computation in polynomial time as efficient will be given once we have defined some basic complexity classes in section \ref{susec:basic-complexity-classes}.

\begin{claim}[Extended Church-Turing Thesis]
  Every physically realizable model of computation can be simulated by a Turing machine in polynomial time, that is there is a Turing machine $\TM$ a polynomial $p$ such that in order to simulate $t$ execution steps of the model, $\TM$ will need at most $p\left(t\right)$ steps.
\end{claim}

This is the extended (or strong) Church-Turing thesis as stated in \cite{aroraComputationalComplexityModern2009}. One will also often find it stated as \enquote{every intuitively realizable model of computation can be efficiently simulated by a Turing machine}. While there exists no general proof of the Church-Turing thesis, and its somewhat non-rigorous statement makes it insusceptible to mathematical proof, once one considers a specific model of computation, this equivalence of power can be proven. Note that there are models of computation that are not just differing definitions of Turing machines (for example, every programming language is also a model of computation), which are, of course, also covered by the Church-Turing thesis. See for example section 1.2.1 of \cite{aroraComputationalComplexityModern2009} for an outline of a proof that Turing machines can simulate a general programming language.

Only the advent of quantum computing and the possibility that of a physically realizable universal quantum computer has brought forward a likely candidate for a model of computation that is strictly more powerful than Turing machines. At this point in time, however, this has not been formally proven, and it is still an open research question as to whether quantum computers are more powerful than Turing machines. For more information, see Scott Aaronson's survey \cite{aaronsonNP2017} \cite{aaronsonNP2017}, specifically section 5.5, or chapter 10 in Arora's book. Still, even with the advent of quantum computing, the extended Church-Turing thesis is still widely held to be true if one only considers models of classical computation, that is models of the kind of deterministic digital computation one finds in modern-day regular computers.

To close this chapter, we will return to the notion of \enquote{algorithm} and how Turing machines actually offer a framework to formalize it. In the following, for simplicity's sake, we will only consider Turing machines with a single halting state which is neither accepting nor rejecting. This does, of course, in no way influence the computational power of the definition.\footnote{For a Turing machine $\TM$ with an accepting and rejecting state, we can construct an equivalent Turing machine $\TM^{\prime}$ with a singular halting state by turning the accepting and rejecting state into non-halting ones, adding a new halting state $q_{\mathsf{end}}$ and a set of transitions such that on reaching the old accepting state, the Turing machine $\TM^{\prime}$ empties the entire tape except for a special accepting symbol, e.g. a \enquote{1}, in a single cell (and, accordingly, new transitions from the old rejecting state that empty the tape except for a single rejecting symbol) and then halts. We can then say that $\TM^{\prime}$ accepts an input $\omega$ if it halts with output 1 (or rejects it if it halts with output 0).}

Note that the entire \enquote{computational information} of a Turing machine is encoded in its transition function. Like the integrated circuits one would find in an old pocket calculator, they are very much single-purpose -- a Turing machine can perform exactly the one kind of calculation described by its transition function. But this is not how a modern computer works. Given that we want Turing machines to offer a theoretical model that allows us to argue about the computational capacities of a computer, the question as to whether we can construct a Turing machine that can calculate anything offers itself up. The answer is yes.\footnote{While this may not be surprising in the day and age of ubiquitous universal computing, it was considered to be a most remarkable result when it was first proven by Turing in 1936.} The reason for this is that, due to its finite size, the transition function offers itself up to be written as a single string, which can then be given to another Turing machine as input. Then, we only need to construct a Turing machine whose \enquote{one kind of computation} it can perform is other Turing machines. More precisely, a Turing machine that, on an input consisting of a string encoding another Turing machine and some input string $\omega$, will simulate the encoded Turing machine on the input $\omega$ efficiently. This then uniquely identifies a Turing machine with what can be thought of as an executable bit string consisting of a sequence of instructions describing how to perform a certain kind of calculation. Or in simpler terms: a program.

\deftion{Turing Machines as Programs}{
  We now show how any Turing machine can be encoded as a bit string, that is an object $\alpha\in\set{0,1}^\ast$. While the choice of alphabet is somewhat arbitrary, it has established itself as a standard in complexity theory, and some textbooks, such as \cite{aroraComputationalComplexityModern2009}, do not even define languages and Turing machines over any other alphabet.

  Let $\TM$ be a Turing machine consisting of
  \begin{itemize}
    \item the set of states $Q=\set{q_{0},\dots,q_{n}}$,
    \item the input alphabet $\Sigma = \set{a_{1},\dots,a_{m}}$,
    \item the tape alphabet $\Gamma = \set{a_{1},\dots,a_{m}, a_{m+1},\dots, a_{k} = \blank}$,
    \item the transition function $\delta\colon Q\times\Sigma\rightarrow Q\times\Gamma\times\set{L,R,N}$,
    \item the starting state $q_{\mathsf{start}}\in Q$,
    \item a single halting state $q_{\mathsf{halt}}\in Q$.
  \end{itemize}
  In order to encode $\delta$ as a string, we note that we can view it as a subset $Q^{2}\times\Sigma\times\Gamma\times D \supset \Delta\coloneqq\set{\brc{q_{i},q_{j},a_{s},a_{t},d_{\ell}}\mid\delta\left(q_{i},a_{s}\right) = \brc{q_{j},a_{t},d_{\ell}}}$, where $D=\set{d_{1},d_{2},d_{3}}$ with $d_{1}=L$, $d_{2}=R$ and $d_{3}=N$. We introduce an ordering $\prec$ on $\Delta$ with
  \[
    \brc{q_{i},q_{j},a_{s},a_{t},d_{\ell}}\prec\brc{q_{r},q_{k},a_{s^{\prime}},a_{t^{\prime}},d_{\ell^{\prime}}}\coloniff i\leq r \text{ and } j \leq k.
  \]
  Note that this is a total order and since $\Delta$ is finite, it has a minimal element. We can therefore use it to enumerate the elements in $\Delta$. Should two elements $d_{1}, d_{2}$ be two transitions from the same state into the same state, we order them via the index of the input symbol. We encode an element $\brc{q_{i},q_{j},a_{s},a_{t},d_{\ell}}\in\Delta$ as the bit string $0^{i}10^{s}10^{j}10^{t}10^{\ell}$ and write $code_{k}$ for it, where $k$ is the index of the element in the enumeration given by $\prec$. This allows us to encode $\Delta$ by concatenating all the bit strings in the order defined by $\prec$ with sequences of 1's as delimiters, resulting in a string
  \[
    111code_{1}11code_{2}11\dots11code_{|\Delta|}111.
  \]
  Furthermore, by convention, it is often assumed that $q_{0}=q_{\mathsf{start}}$ and $q_{1}=q_{\mathsf{halt}}$ (since we can always just re-index the states), so that these get encoded at the start. Note that this, like the enumerating we defined above, is not a necessary convention -- even with an arbitrary enumeration of the elements in $\Delta$, the construction would still work. It does, however, make the following proofs a bit less messy.

  We can now describe any Turing machine as an element $\alpha\in\set{0,1}^{\ast}$. We now introduce two further useful conventions. The first is that any string that is not a well-formatted encoding of a Turing machine is an encoding of the Turing machine that immediately halts with output 0. The second one is that if $\alpha$ is an encoding of a Turing machine, $\alpha1^{\ast}$ is an encoding of the same Turing machine. This is to say we can add any amount of trailing ones to an encoding of a Turing machine without changing its computational content. Finally, we write $\TM_{\alpha}$ for the Turing machine encoded by the string $\alpha$. The two previous conventions guarantee that this is always a meaningful piece of notation, no matter the string $\alpha$.
}

\thrm{The Universal Turing Machine}{
  There exists a \emph{universal Turing machine} $\TM_U$ with input alphabet $\set{0,1}$ and tape alphabet $\set{\text{\blank}, 0, 1}$ such that for every $\omega,\alpha\in\set{0,1}^\ast$, on the input $(\alpha, \omega)$, $\TM_{U}$ will simulate $\TM_{\alpha}$ on the input $\omega$. Moreover, this is an efficient simulation. If $\TM_{\alpha}$ halts on the input $\omega$ within $t$ steps, then $\TM_U$ halts on $(\omega,\alpha)$ within $c \cdot t \cdot \log t$ steps, where $c$ depends not on $\omega$ but only on the size of $\TM_{\alpha}$.
}
\prf{
  For a detailed proof, see section 1.7 in \cite{aroraComputationalComplexityModern2009}, we will only give an outline here. For this, we will assume that the Turing machine encoded in the input also uses the binary alphabet, which we will justify afterwards. The rough idea of how the universal Turing machine works is as follows. It reads the description and input and then saves the state-transition function of the Turing machine to be simulated as a look-up table on its tape. Furthermore, it saves the current state and head position of the simulated Turing machine as well. Then it just starts working on input: it reads the first symbol, checks the current state, looks up the transition from the current state on the input symbol, performs it, updates head position, writes new symbol, updates the current state of the simulated Turing machine etc.
}

\begin{lemma}
  For any Turing machine $\TM$, one can construct an equivalent Turing machine $\tilde{\TM}$ that uses a binary input alphabet and carries the same computational info as $\TM$.
\end{lemma}
\prf{
  We only give a quick sketch of the proof, by outlining how such a Turing machine $\tilde{\TM}$ can be constructed. The idea is to replace each input symbol with a $2^{\lceil\log k\rceil}$ bit string, where $k$ is the length of the input alphabet of $\TM$. We then extend the amount of states and the state transition function such that $\tilde{\TM}$ can \enquote{remember} the bits of the string encoding the input symbol which it has already scanned and perform the state transition function of $\TM$ \enquote{bit-wise}. This construction yields a Turing machine performing a computation equivalent to that of $\TM$ and using the binary alphabet.
}

%% TODO here insert two more examples that also illustrate the different levels of abstraction one can use when talking about turing machines, see Sipser 3.7
%% TODO note that these are not the only levels of abstraction - one could als describe a TM in pseudocode, for example
%% TODO move this up in the text to the appropriate position
%Sipser \cite{sipserIntroductionTheoryComputation2012} describes three levels of abstraction one can use when arguing about Turing machines. The lowest one is describing a Turing machine by explicitly writing out its state transition function. After that, one \enquote{abstracts} from it by describing how the read-write head interacts with and moves around the tape in general language instead of the more formal description of state transition tables or automata diagrams. Finally, the highest level of abstraction (and the one most often used in practice) is describing the computation the machine performs in everyday language. To develop a better feel for these different levels of abstraction and how they interact, we will now give two examples of Turing machines which we describe on all of the three levels each.
%
%\begin{example}[Incrementation as a Turing Machine]
%  We describe a Turing machine that takes a bit-string as an input, interprets it as the binary representation of a number -- read from left to right -- and increments it by one. This example was borrowed from \cite{wagnerVorlesungsskriptTheoretischeGrundlagenWS11/12}.
%
%  Level 3 (normal language)
%
%  Level 2 (tape behaviour)
%
%  Level 1 (state automata)
%  \begin{figure}
%    \[\begin{tikzpicture}
%      \tikzstyle{every path}=[very thick]
%      \begin{scope}
%        \tikzset{
%          ->, % makes the edges directed
%          node distance=2cm, % specifies the minimum distance between two nodes. Change if necessary.
%          every state/.style={thick, fill=gray!10}, % sets the properties for each ’state’ node
%          initial text=$ $, % sets the text that appears on the start arrow
%        }
%
%        \node[state, initial] (q0) at (2,-6) {$q_{0}$};
%        \node[state, right = of q0] (q1) {$q_{1}$};
%        \node[state, right = of q1] (q2) {$q_{2}$};
%        \node[below = of q2] (dummy2) {};
%        \node[state, right = of dummy2] (q3) {$q_{3}$};
%        \node[state, below = of q0] (q4) {$q_{4}$};
%        \node[state, below = of q4] (q5) {$q_{5}$};
%        \node[right = of q5] (dummy) {};
%        \node[state, accepting, right = of dummy] (q6) {$q_{6}$};
%
%        \draw (q0) edge[below] node{1|1, R} (q1)
%              (q1) edge[loop above] node{$\begin{matrix}0|0, R \\ 1|1, R\end{matrix}$} (q1)
%              (q1) edge[below] node{\blank|\blank, L} (q2)
%              (q2) edge[loop above] node{1|0, L} (q2)
%              (q0) edge[left] node{0|0, R} (q4)
%              (q4) edge[left] node{\blank|\blank, L} (q5)
%              (q5) edge[above] node{0|1, L} (q6)
%              (q2) edge[above] node{0|1, L} (q3)
%              (q3) edge[loop right] node{$\begin{matrix}0|0, L \\ 1|1, L\end{matrix}$} (q3)
%              (q3) edge[right] node{\blank|\blank, L} (q6);
%
%%        \node[rounded corners,draw=black,thick,fit=(q0) (q1) (q2) (q3) (q4) (q5) (q6) (q7) (dummy) (dummy2) {};
%      \end{scope}
%    \end{tikzpicture}\]
%  \caption{Test}
%  \end{figure}
%\end{example}
%
%\ex{A Turing Machine Recognizing \(\set{0^{2^{n}}\mid n\in\mathbb{N}}\)}{
%  Take Sipser 3.7
%}
%
%% TODO or maybe Sipser 3.11?


