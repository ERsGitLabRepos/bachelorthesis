Having laid the groundwork of developing a model of computation and formalizing what it means for a problem to be solvable by an algorithm, we now properly enter the subject area of complexity theory. Complexity theory is interested in classifying problems by the efficiency properties of the algorithms that solve them, or, spoken more casually, by how hard they are. This desire naturally gives rise to the concept of complexity classes, in which we collect problems of similar difficulty. While we will only consider classifications of runtime efficiency measured in required steps of computation, other kinds of complexity, such as space complexity, also exist.

\deftion{Computable Function}{
  A function $f:\Sigma^{\ast}\rightarrow\Gamma^{\ast}$ is called \emph{computable} if there exists a Turing machine $\TM_{f}$ that halts on any input $\omega\in\Sigma^\ast$ with output $f\left(\omega\right)$.

  If $\TM_{f}$ is polynomial time, then we say $f$ is \emph{computable in polynomial time}.
}

\deftion{Decidable Languages}{
  A language $L$ is called \emph{decidable} if its characteristic function is computable. We then say that the Turing machine computing the characteristic function \emph{decides} the language (or the associated decision problem).
}

\deftion{Semi-decidable Language}{
  A language $L$ is called \emph{semi-decidable} if there is a Turing machine $\TM_{L}$ that accepts any $\omega\in L$, but which may loop indefinitely on an $\omega\notin L$. We then sometimes say that $\TM_{L}$ \emph{verifies} (or \emph{recognizes}) $L$.
}

\deftion{The Class $\P$ of Problems}{
  The class $\P$ is the class of languages (or decision problems) $L$ for which there exists a Turing machine $\TM_{L}$ and a polynomial $p$ such that for each $\omega\in\Sigma^\ast$, $\TM_{L}$ decides whether $\omega\in L$ in at  most $p\left(|\omega|\right)$ steps. In other words, the Turing machine $\TM_{L}$ decides $L$ in polynomial time.
}

\ex{$\mathsc{Primes}$ is in $\P$}{
  We return to example \ref{ex-1:primes} one last time. By our previous note on encoding, we can view this as just the language of all prime numbers or equivalently the decision problem of whether a given natural numbers is prime. As proven in \cite{agrawalPRIMES2004}, this problem is in $\P$.
}

As noted before, computation in polynomial time is often viewed as \enquote{efficient computation} in practice. Therefore, the problems in $\P$ are often described as the problems which we can solve efficiently in practice. Of course, a polynomial of degree 1000 is still a polynomial, and if one had a problem that was decided by a Turing machine whose runtime was a polynomial of such high degree, its large instances would take an infeasible amount of time to compute in practice. One might therefore justifiably question the identification of \enquote{computable in polynomial time} with \enquote{efficiently computable}. Thus, it is therefore a most remarkable fact that for almost all problems in $\P$, especially the ones of practical concern, there exist algorithms that run in at most cubic time \cite{aaronsonNP2017}, making this identification a lot more reasonable. We will now introduce another important class of problems, for which it is not known whether or not they can be efficiently solved.

\deftion{The Class $\NP$ of Problems}{
  The class $\NP$ is the class languages (or decision problems) $L$ for which there exists a Turing machine $\TM_{L}$ and a polynomial $p$ such that for all $\omega\in\Sigma^\ast$, $\omega\in L$ if and only if there exists a \emph{witness} $\mathsf{w}\in\Sigma^{p\left(|\omega|\right)}$ (sometimes also referred to as a \emph{certificate}) such that $\TM_{L}$ accepts $\left(\mathsf{w},\omega\right)$, the concatenation of $\mathsf{w}$ and $\omega$ separated by a delimiter symbol, in polynomial time depending \emph{only} on $|\omega|$. We then say that $\TM_{L}$ \emph{verifies} $L$.
}

For instances of problems in $\P$, witness strings always exist. Indeed, any string is a witness string of any instance of any problem in $\P$. This is since there exists a Turing machine which decides the problem, so we can construct one that verifies the problem by constructing a Turing machine that first discards the witness and then just decides the input. In other words, for a language $L\in\P$ the empty string $\epsilon$ is a witness for any $\omega\in L$ (and any $\alpha\in L^{\complement}$). Thus, $\P$ is in $\NP$.

It is often helpful to think of the witness as a suggested solution to the problem instance, and this is how candidates for witness strings are usually chosen in practice. The definition above can therefore be understood as the problems in $\NP$ being the problems for which a solution can be verified in polynomial time. It is an open problem in complexity theory whether the inclusion $\P \subset \NP$ is a proper one, i.e. whether there are problems for which there exists no polynomial-time algorithm. However, this is widely assumed to be true (for example in all of modern cryptography). A thorough survey of the current state of research on that question can be found in \cite{aaronsonNP2017}.

In complexity theory, we are interested in classifying problems by their difficulty. One way of doing this is to compare a problem's difficulty to that of a problem where we already know \enquote{how hard} it is. The primary theoretical tool to do this is the proof technique of a Turing reduction. We will, however, not discuss Turing reductions in the entirety of their formal depth and instead focus on a slightly weaker variant usually referred to as a \emph{many-one reduction}, as it is sufficient for the purposes of this thesis.

\deftion{Many-One Reduction}{
  Let $\mathsc{Pbm}, \mathsc{Qst}$ be two languages over the same alphabet $\Sigma$. A \emph{many-one reduction} from $\mathsc{Pbm}$ to $\mathsc{Qst}$ is a computable function $\mathsf{M}\colon\Sigma^{\ast}\rightarrow\Sigma^{\ast}$ such that $\omega\in\mathsc{Pbm}\iff\mathsf{M}\left(\omega\right)\in\mathsc{Qst}$. We then say that $\mathsc{Pbm}$ is reducible to $\mathsc{Qst}$ or that $\mathsc{Qst}$ is \enquote{at least as hard as} $\mathsc{Pbm}$.

  A reduction $\mathsf{M}$ is called \emph{polynomial} if there exists a Turing machine $\TM$ that computes $\mathsf{M}$ in polynomial time.
}

\deftion{$\NP$-Hardness}{
  A problem $\mathsc{Pbm}$ is called $\NP$-hard if it is at least as hard as any other problem in $\NP$, that is if for any other problem $\mathsc{T}\in\NP$, there exists a Turing reduction from $\mathsc{T}$ to $\mathsc{Pbm}$.
}

Note that to show that a problem is $\NP$-hard, it suffices to construct a reduction to another $\NP$-hard problem, as polynomial reductions are closed under composition.

\deftion{$\NP$-Completeness}{
  A problem $\mathsc{Pbm}$ is called $\NP$-complete if it is $\NP$-hard and $\mathsc{Pbm}\in\NP$.
}

A lot of $\NP$-hard problems that are considered to be of practical importance are also known to be $\NP$-complete. We will conclude this chapter by giving just three examples, the latter two of which we will use in the proofs of sections \ref{sec:cohomology-of-finite-type} to \ref{sec:betti-cup-elliptic}.

% TODO examples - kSat, kCol, SubsetSum

\begin{problem}[$\mathsc{kSat}$]
  % TODO do i maybe need to explain this in more depth?
  $\mathrm{Instance\colon}$ A boolean formula in conjunctive normal form with $k$ literals per clause,
  \[
    \brc{x_{1_{1}}\vee x_{1_{2}} \vee \cdots \vee x_{1_{k}}} \wedge \brc{x_{2_{1}}\vee x_{2_{2}} \vee \cdots \vee x_{2_{k}}} \wedge \cdots \wedge \brc{x_{j_{1}}\vee x_{j_{2}} \vee \cdots \vee x_{j_{k}}}.
  \]
  $\mathrm{Question\colon}$ Is there a configuration -- that is a mapping that assigns each variable a boolean truth value $\mathsc{true}$ or $\mathsc{false}$ --, such that the formula is satisfied?
\end{problem}

This problem was first proved to be $\NP$-complete by S.A. Cook in his landmark paper \cite{cookComplexityTheoremprovingProcedures1971}. It was in this paper that the notion of $\NP$-completeness was first defined. One year later, R.M. Karp proved the $\NP$-completeness of a further 21 important problems using reductions to the boolean satisfiability problem in \cite{karpReducibilityCombinatorialProblems1972}, including the following two examples. For these reasons, boolean satisfiability is often considered to be the \enquote{grandfather} of $\NP$-complete problems.

\begin{problem}[$\mathsc{kCol}$]
  $\mathrm{Instance\colon}$ A graph $G\coloneqq\brc{V,E}$.\newline
  $\mathrm{Question\colon}$ Does $G$ allow a proper $k$-coloring, that is exists there a function $\kappa\colon V\rightarrow\set{1,\dots,k}$ such that $\kappa(v_{i}) \neq \kappa(v_{j})$ for all $\brc{v_{i},v_{j}}\in E$?
\end{problem}

\begin{problem}[$\mathsc{SubsetSum}$]
  $\mathrm{Instance\colon}$ A pair consisting out of a multiset $M$ of integers and a target integer $t$.\newline
  $\mathrm{Question\colon}$ Does there exist a subset $S$ of $M$ such that the elements of $S$ add up to $t$?
\end{problem}



